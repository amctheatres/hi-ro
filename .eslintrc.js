module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
        mocha: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:jsx-a11y/recommended',
        'plugin:react/recommended',
    ],
    globals: {
        __DEV__: true,
    },
    parser: 'babel-eslint',
    parserOptions: {
        ecmaFeatures: {
            generators: false,
            jsx: true,
            objectLiteralDuplicateProperties: false,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [ 'import', 'jsx-a11y', 'react' ],
    rules: {
        'import/no-named-as-default': [ 'warn' ],
        'no-console': [ 'warn' ],
        'no-unused-vars': [ 'warn' ],
    },
    settings: {
        'import/extensions': [ '.js' ],
        'import/resolver': {
            node: {
                extensions: [ '.js', '.json' ],
            },
            webpack: {
                config: 'webpack/base.config.js',
            },
        },
        react: {
            version: '15.4',
        },
    },
};
