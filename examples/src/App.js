import React from 'react';
import ReactDOM from 'react-dom';
import { Editor, Content } from '../../src';

class App extends React.Component {

    constructor(props) {
        super(props);

        this.html = '<p><h2>HTML Initial Value</h2>Para <strong>Text</strong></p>';
        this.json = {
            type: 'div',
            children: [
                {
                    type: 'h2',
                    children: [
                        { __text: 'JSON Initial Value' }
                    ]
                },
                { __text: '\n' },
                {
                    type: 'p',
                    children: [
                        { __text: 'Para ' },
                        {
                            type: 'strong',
                            children: [
                                { __text: 'Text' }
                            ]
                        }
                    ]
                }
            ]
        };

        this.state = {
            editorContent: this.json,
            json: '',
            content: null
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(content) {
        this.setState({
            editorContent: content,
            json: JSON.stringify(content, null, 2),
            content
        });
    }

    html;
    json;

    handleDefaultValue(type) {
        if (type === 'html') {
            this.setState({ editorContent: this.html });
        } else if (type === 'json') {
            this.setState({ editorContent: this.json });
        }
    }

    render() {
        const editorConfig = {
            browser_spellcheck: true,
            plugins: 'autolink link image lists print preview code',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright code',
            menubar: 'tools'
        };
        return (
            <div>
                <button onClick={this.handleDefaultValue.bind(this, 'html')}>Html Initial value</button>
                <button onClick={this.handleDefaultValue.bind(this, 'json')}>Json Initial value</button>
                <h1>Editor</h1>
                <Editor onChange={this.handleChange} value={this.state.editorContent} editorConfig={editorConfig}/>
                <h1>Output</h1>
                <h2>JSON</h2>
                <pre style={{ height: 400, overflowX: 'auto', border: 'inset 2px #ddd' }}>{this.state.json}</pre>
                <h2>Components</h2>
                <Content content={this.state.content}/>
            </div>
        );
    }
}
document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(<App />, document.getElementById('example'));
});
