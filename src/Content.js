import React from 'react';
import PropTypes from 'prop-types';

import createElements from './createElements';
import HtmlPropTypes from './HtmlPropTypes';

class HiRoContent extends React.Component {
    shouldComponentUpdate(nextProps) {
        return !(this.props.contentId && nextProps.contentId && this.props.contentId === nextProps.contentId);
    }

    render() {
        return createElements(this.props.content);
    }
}

HiRoContent.propTypes = {
    content: HtmlPropTypes,
    contentId: PropTypes.string
};

export default HiRoContent;
