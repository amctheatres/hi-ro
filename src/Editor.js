import PropTypes from 'prop-types';
import React from 'react';
import TinyMce from 'react-tinymce-input';
import ReactDOMServer from 'react-dom/server';

import transform from './transform';
import createElements from './createElements';
import HtmlPropTypes from './HtmlPropTypes';

class HiRoEditor extends React.Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(html) {
        const { onChange, transformConfig, onTransformElement } = this.props;
        const config = Object.assign({
            visitElement: onTransformElement
        }, transformConfig || {});

        const element = transform(html, config);

        if (onChange) onChange(element);
    }

    passThroughProps(props) { // eslint-disable-line class-methods-use-this
        return Object.keys(TinyMce.propTypes)
            .filter(key => ({}.hasOwnProperty.call(props, key) && !{}.hasOwnProperty.call(HiRoEditor.propTypes, key)))
            .reduce((p, key) => {
                p[key] = props[key];
                return p;
            }, {});
    }

    render() {
        let { value, editorConfig, name, ignoreUpdatesWhenFocused = true } = this.props; // eslint-disable-line prefer-const

        if (value && typeof value !== 'string') {
            const element = createElements(value);

            if (element) {
                value = ReactDOMServer.renderToStaticMarkup(element);
            }
        }

        value = value || '';

        const config = Object.assign({
            browser_spellcheck: true,
            plugins: 'autolink link image lists print preview',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
        }, editorConfig || {});

        const passThrough = this.passThroughProps(this.props);

        return (
            <div>
                <TinyMce
                    {...passThrough}
                    name={name}
                    value={value}
                    tinymceConfig={config}
                    onChange={this.handleChange}
                    ignoreUpdatesWhenFocused={ignoreUpdatesWhenFocused}
                />
            </div>
        );
    }
}

HiRoEditor.propTypes = {
    name: PropTypes.string,
    value: HtmlPropTypes,
    onTransformElement: PropTypes.func,
    onChange: PropTypes.func,
    editorConfig: PropTypes.object,
    transformConfig: PropTypes.object,
    ignoreUpdatesWhenFocused: PropTypes.bool,
};

export default HiRoEditor;
