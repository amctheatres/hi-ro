import { PropTypes } from 'prop-types';

const HtmlPropTypes = PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
        __text: PropTypes.string.isRequired,
    }),
    PropTypes.shape({
        type: PropTypes.string.isRequired,
        props: PropTypes.object,
        children: PropTypes.array
    }),
    PropTypes.object
]);

export default HtmlPropTypes;
