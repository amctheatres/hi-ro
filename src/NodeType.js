// https://developer.mozilla.org/en-US/docs/Web/API/Node.nodeType

const NodeType = {
    Element: 1,
    Text: 3
};

export default NodeType;