import toReactAttributeName from 'react-attr-converter';

import NodeType from './NodeType';
import { isBooleanAttribute } from './attributes';
import { mergeAndConcat } from './utils';

class Visitor {
    constructor(opts) {
        this.options = mergeAndConcat({
            excludeTypes: ['style'],
            excludeProps: [],
            excludeStyles: [],
            elementAttributeMapping: {
                input: {
                    checked: 'defaultChecked',
                    value: 'defaultValue'
                }
            },
            visitElement: null,
            visitText: null,
            visitProp: null,
            visitStyle: null
        }, opts);
    }

    visit(node, parentContext = null) {
        let output;

        switch (node.nodeType) {
            case NodeType.Element:
                output = this.visitElement(node, parentContext);
                break;
            case NodeType.Text:
                output = this.visitText(node, parentContext);
                break;
            default: // includes comments
                return null;
        }

        if (output == null) return null;

        return output;
    }

    visitElement(node, parentContext = null) {
        const type = node.tagName.toLowerCase();

        if (this.options.excludeTypes.indexOf(type) > -1) return null;

        const output = {
            type,
            children: []
        };

        const context = {
            node,
            output,
            parent: parentContext,
            children: []
        };

        const props = this.getProps(type, node, context) || {};

        if (type === 'textarea' && typeof props.defaultValue === 'undefined') {
            // Hax: textareas need their inner text moved to a "defaultValue" prop.
            props.defaultValue = JSON.stringify(node.value);
        }

        if (props && Object.keys(props).length) {
            output.props = props;
        }

        // children
        for (let i = 0, count = node.childNodes.length; i < count; i++) {
            const child = this.visit(node.childNodes[i], context);

            if (child) {
                output.children.push(child);
            }
        }

        if (!output.children.length) {
            delete output.children;
        }

        if (this.options.visitElement && this.options.visitElement(context) === false) {
            return null;
        }

        return context.output;
    }

    visitText(node, parentContext = null) {
        if (node.parentNode && node.parentNode.tagName.toLowerCase() === 'textarea') {
            // Ignore text content of textareas, as it will have already been moved to a "defaultValue" prop.
            return null;
        }

        const context = {
            node,
            output: {
                __text: node.textContent
            },
            parent: parentContext
        };

        if (this.options.visitText && this.options.visitText(context) === false) {
            return null;
        }

        if (!context.output) return null;

        return context.output;
    }

    getProps(type, node, parentContext = null) {
        const props = {};

        for (let i = 0, count = node.attributes.length; i < count; i++) {
            const attribute = node.attributes[i];

            let key;
            let value;

            if (attribute.name === 'style') {
                key = 'style';
                value = this.parseStyles(type, node, attribute.value, parentContext);
            } else {
                const elMapping = this.options.elementAttributeMapping[type];

                if (elMapping) {
                    key = elMapping[attribute.name];
                }

                if (!key) {
                    key = toReactAttributeName(attribute.name || '');
                }

                if (!key) {
                    key = attribute.name;
                }

                if (!key) continue;

                value = attribute.value;

                if (isBooleanAttribute(key) && !value) {
                    value = true;
                }
            }

            if (this.options.excludeProps.indexOf(key) > -1) continue;

            const pair = {
                key,
                value
            };

            const context = {
                type,
                node,
                output: pair,
                parent: parentContext
            };

            if (this.options.visitProp && this.options.visitProp(context) === false) {
                continue;
            }

            props[pair.key] = pair.value;
        }

        return props;
    }

    parseStyles(type, node, rawStyle, parentContext = null) {
        const styles = {};
        rawStyle.split(';').forEach((style) => {
            style = style.trim();
            const firstColon = style.indexOf(':');

            let pair = {
                key: null,
                value: null
            };

            pair.key = style.substr(0, firstColon);
            pair.value = style.substr(firstColon + 1).trim();

            if (!pair.key) return;

            // don't capitalize ms
            if (/^-ms-/.test(pair.key)) {
                pair.key = pair.key.substr(1);
            }

            // camelcase
            pair.key = pair.key.replace(/-(.)/g, (match, chr) => {
                return chr.toUpperCase();
            });

            if (this.options.excludeStyles.indexOf(pair.key) > -1) return;

            // replace pixels
            pair.value = pair.value.replace(/^(\d+)px$/, '$1');

            const context = {
                type,
                node,
                output: pair,
                parent: parentContext
            };

            if (this.options.visitStyle && this.options.visitStyle(context) === false) {
                return;
            }

            pair = context.output;

            styles[pair.key] = pair.value;
        });

        return styles;
    }
}

export default Visitor;
