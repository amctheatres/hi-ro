const reactHtmlBooleans = new Set([
    'allowFullScreen',
    'async',
    'autoFocus',
    'autoPlay',
    'controls',
    'default',
    'defer',
    'disabled',
    'formNoValidate',
    'hidden',
    'loop',
    'noModule',
    'noValidate',
    'open',
    'playsInline',
    'readOnly',
    'required',
    'reversed',
    'scoped',
    'seamless',
    'itemScope',
]);

const reactDomPropertyBooleans = new Set([ 'checked', 'multiple', 'muted', 'selected' ]);

const reactOverloadedBooleans = new Set([ 'capture', 'download' ]);

export const isBooleanAttribute = (reactAttributeName) =>
    reactHtmlBooleans.has(reactAttributeName) ||
    reactDomPropertyBooleans.has(reactAttributeName) ||
    reactOverloadedBooleans.has(reactAttributeName);
