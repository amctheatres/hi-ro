import React from 'react';
import { warn } from './utils';

export default function createElements(serializedElement, options, index, parentContext = null) {
    if (!serializedElement) {
        return null;
    }

    if (Array.isArray(serializedElement)) {
        warn('Element cannot be an array and must have a single root item');
        return null;
    }

    if (typeof serializedElement === 'string') {
        return serializedElement;
    }

    if (!!serializedElement && serializedElement.__text) {
        return serializedElement.__text;
    }

    index = index || 0;
    options = options || {};

    let { type, props } = serializedElement; // eslint-disable-line prefer-const

    if (!type) return null;

    props = props || {};

    if (!props.key) {
        props.key = index;
    }

    const context = {
        type,
        props,
        children: []
    };

    if (parentContext) {
        context.parent = {
            type: parentContext.type,
            props: parentContext.props,
            children: parentContext.children,
        };
    }

    let elementChildren = serializedElement.children;
    if (elementChildren) {
        if (!Array.isArray(elementChildren)) {
            elementChildren = [elementChildren];
        }

        if (elementChildren.length) {
            elementChildren.forEach((childElement, i) => {
                const child = createElements(childElement, options, i, context);
                context.children.push(child);
            });
        }
    }

    if (options.visitElement) {
        if (options.visitElement(context) === false) return null;
    }

    let element = null;

    try {
        element = React.createElement(context.type, context.props, context.children.length ? context.children : null);
    } catch (err) {
        let t = context.type || 'unknown';

        if (typeof t !== 'string') {
            t = t.displayName || t.toString();
        }

        warn(`Error when creating react element <${t}>`);
    }

    return element;
}
