import Visitor from "./Visitor";
import NodeType from "./NodeType";

// basis for this comes from https://github.com/reactjs/react-magic/blob/master/src/htmltojsx.js
let document;

export default function transform(html, options) {
  const container = getContainer(html);

  const visitor = new Visitor(options);

  return visitor.visit(container);
}

function getContainer(html) {
  const container = create("div");

  container.innerHTML = (html || "")
    .trim()
    .replace(/<script([\s\S]*?)<\/script>/g, "");

  if (
    container.childNodes.length === 1 &&
    container.childNodes[0].nodeType === NodeType.Element
  ) {
    return container.childNodes[0];
  }

  return container;
}

function create(tag) {
  if (!global.window) {
    if (!document) {
      const { JSDOM } = require("jsdom");

      document = new JSDOM().window.document;
    }
  } else {
    document = window.document;
  }

  return document.createElement(tag);
}
