import merge from 'lodash.merge';

export function mergeAndConcat(...args) {
    // eslint-disable-next-line consistent-return
    const concat = (a, b) => {
        if (Array.isArray(a)) {
            return a.concat(b);
        }
    };

    const dest = {};

    for (let i = 0; i < args.length; i++) {
        merge(dest, args[i], concat);
    }

    return dest;
}

export function warn(msg) {
    const message = `Warning: ${msg}`;
    if (typeof console !== 'undefined') {
        // eslint-disable-next-line no-console
        console.error(message);
    }
}
