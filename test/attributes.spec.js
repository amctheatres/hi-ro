import { isBooleanAttribute } from '../src/attributes';

describe('isBooleanAttribute', () => {
    it('is true when the attribute name is in one of the sets of react booleans', () => {
        // html boolean
        isBooleanAttribute('autoPlay').should.be.true;

        // dom property boolean
        isBooleanAttribute('checked').should.be.true;

        // overloaded boolean
        isBooleanAttribute('download').should.be.true;
    });

    it('is false when the attribute name is not in the set of react booleans', () => {
        isBooleanAttribute(null).should.be.false;
        isBooleanAttribute().should.be.false;
        isBooleanAttribute('className').should.be.false;
    });
});
