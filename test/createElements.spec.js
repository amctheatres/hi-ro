import React from 'react';
import { createElements } from '../src';

const should = require('chai').should();

describe('createElements', () => {
    it('handles null input', () => {
        const elements = createElements(null);

        should.not.exist(elements);
    });

    it('handles empty input', () => {
        const elements = createElements('');

        should.not.exist(elements);
    });

    it('handles empty object input', () => {
        const elements = createElements({});

        should.not.exist(elements);
    });

    it('creates div', () => {
        const serialized = { type: 'div' };

        const elements = createElements(serialized);

        should.exist(elements);
        elements.type.should.equal('div');
    });

    it('creates div with new text format', () => {
        const serialized = { type: 'div', children: [{ __text: 'Text' }] };

        const elements = createElements(serialized);

        console.log(elements);

        should.exist(elements);
        elements.type.should.equal('div');
        elements.props.children[0].should.equal('Text');
    });

    it('does not throw when given bad input', () => {
        const serialized = { type: 'div', children: [{ type: 'p', children: ['Text'] }] };
        class Link extends React.Component {
            render() {
                return (<a href='#'>{this.props.children}</a>);
            }
        }
        const options = {
            visitElement: (ctx) => {
                if (ctx.type === 'p') ctx.type = Link;
            }
        };

        const elements = createElements(serialized, options);

        should.exist(elements);
        elements.props.children[0].type.should.equal(Link);
    });

    it('does not throw when given bad input', () => {
        const serialized = { type: 'div', children: [{ type: 'p', children: ['Text'] }] };
        class Link extends React.Component {
            render() {
                return (<a href='#'>{this.props.children}</a>);
            }
        }
        const options = {
            visitElement: (ctx) => {
                if (ctx.type === 'p') ctx.type = Link;
            }
        };

        const elements = createElements(serialized, options);

        should.exist(elements);
        elements.props.children[0].type.should.equal(Link);
    });

    it('calls visitElement', () => {
        let called = false;
        let type;
        let props;
        let children;
        const serialized = { type: 'p', children: ['Paragraph'] };
        const options = {
            visitElement: (ctx) => {
                called = true;
                type = ctx.type;
                props = ctx.props;
                children = ctx.children;
            }
        };

        const elements = createElements(serialized, options);

        should.exist(elements);
        called.should.be.true;
        should.exist(type);
        should.exist(props);
        should.exist(children);
    });

    it('aborts when visitElement returns false', () => {
        const serialized = { type: 'div', children: [{ type: 'p', children: ['Text'] }] };
        const options = {
            visitElement: (ctx) => {
                if (ctx.type === 'p') return false;
            }
        };

        const elements = createElements(serialized, options);

        should.exist(elements);
        should.not.exist(elements.props.children[0]);
    });

    it('honors changes when visitElement modifies output with simple type', () => {
        const serialized = { type: 'div', children: [{ type: 'p', children: ['Text'] }] };
        const options = {
            visitElement: (ctx) => {
                if (ctx.type === 'p') ctx.type = 'div';
            }
        };

        const elements = createElements(serialized, options);

        should.exist(elements);
        elements.props.children[0].type.should.equal('div');
    });

    it('honors changes when visitElement modifies output with component type', () => {
        const serialized = { type: 'div', children: [{ type: 'p', children: ['Text'] }] };

        class Link extends React.Component {
            render() {
                return (<a href='#'>{this.props.children}</a>);
            }
        }

        const options = {
            visitElement: (ctx) => {
                if (ctx.type === 'p') ctx.type = Link;
            }
        };

        const elements = createElements(serialized, options);

        should.exist(elements);
        elements.props.children[0].type.should.equal(Link);
    });
});