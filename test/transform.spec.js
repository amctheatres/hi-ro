import ReactDOMServer from 'react-dom/server';
import { transform, createElements } from '../src';

const should = require('chai').should();

describe('transformer', () => {
    before(() => {
        // do this so we don't get a completion time warning on the first test.
        require('jsdom');
    });

    it('transforms simple html', () => {
        const html = '<div>Some Text</div>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms html with attributes', () => {
        const html = '<div id="my-tag">Some Text</div>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms html with custom attributes', () => {
        const html = '<div role="menuitemcheckbox" aria-checked="true" data-customdata="my custom data">Some Text</div>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms html with spaces', () => {
        const html = '<div>Some Text <span>Text</span> Text</div>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms html and round trip from json', () => {
        const html = '<div id="my-tag" aria-role="blurf">Some Text</div>';

        const react = transform(html);
        const json = JSON.stringify(react);
        const parsed = JSON.parse(json);
        const element = createElements(parsed);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms nested html', () => {
        const html = '<div id="tag1"><h1>Header Text</h1><p>Some Text</p></div>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms svg', () => {
        const html = '<svg height="30" width="200"><circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red"></circle><text x="0" y="15" fill="red">I love SVG!</text><use xlink:href="symbol.svg"></use></svg>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms boolean attribute with empty string value', () => {
        const html = '<button disabled=""></button>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms XHTML boolean attribute', () => {
        const html = '<button disabled="disabled"></button>';
        const expected = '<button disabled=""></button>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms HTML5 boolean attribute', () => {
        const html = '<button disabled></button>';
        const expected = '<button disabled=""></button>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms boolean attribute when set to "true"', () => {
        const html = '<button disabled="true"></button>';
        const expected = '<button disabled=""></button>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms overloaded boolean attribute if no value exists', () => {
        const html = '<a download></a>';
        const expected = '<a download=""></a>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms overloaded boolean attribute without changing value if value exists with quotes', () => {
        const html = '<a download="fileName"></a>';
        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms overloaded boolean attribute without changing value if value exists without quotes', () => {
        const html = '<a download=fileName></a>';
        const expected = '<a download="fileName"></a>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms overloaded boolean attribute without changing value if value is falsy with quotes', () => {
        const html = '<a download="false"></a>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms overloaded boolean attribute without changing value if value is falsy without quotes', () => {
        const html = '<a download=false></a>';
        const expected = '<a download="false"></a>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms input with value', () => {
        const html = '<input type="text" value="my value"/>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('transforms list without root tag', () => {
        const html = '<p>Paragraph 1</p><p>Paragraph 2</p><p>Paragraph 3</p>';
        const expected = `<div>${html}</div>`;

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('transforms html with inline styles', () => {
        const html = '<p style="font-weight:bold;color:#ffcc00;border:1px solid #000">Paragraph</p>';

        const react = transform(html);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(html);
    });

    it('exclude types', () => {
        const html = '<div><p>Para Text</p><blockquote>Quoted Text</blockquote></div>';
        const expected = '<div><p>Para Text</p></div>';
        const options = {
            excludeTypes: ['blockquote']
        };

        const react = transform(html, options);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('exclude props', () => {
        const html = '<div data-custom-data="custom data" id="my-id"></div>';
        const expected = '<div id="my-id"></div>';
        const options = {
            excludeProps: ['data-custom-data']
        };

        const react = transform(html, options);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('exclude styles', () => {
        const html = '<div style="color:#000;background-color: #000"></div>';
        const expected = '<div style="background-color:#000"></div>';
        const options = {
            excludeStyles: ['color']
        };

        const react = transform(html, options);
        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('calls visitElement', () => {
        let called = false;
        let node;
        let output;
        const html = '<div></div>';
        const options = {
            visitElement: (ctx) => {
                called = true;
                node = ctx.node;
                output = ctx.output;
            }
        };

        const react = transform(html, options);

        should.exist(react);
        called.should.be.true;
        should.exist(node);
        should.exist(output);
    });

    it('aborts when visitElement returns false', () => {
        const html = '<div><p></p></div>';
        const expected = '<div></div>';
        const options = {
            visitElement: (ctx) => {
                if (ctx.output.type === 'p') return false;
                return undefined;
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('honors changes when visitElement modifies output', () => {
        const html = '<div><p>Text</p></div>';
        const expected = '<div><div>Text</div></div>';
        const options = {
            visitElement: (ctx) => {
                if (ctx.output.type === 'p') ctx.output.type = 'div';
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('calls visitText', () => {
        let called = false;
        let node;
        let output;
        const html = '<div>Text</div>';
        const options = {
            visitText: (ctx) => {
                called = true;
                node = ctx.node;
                output = ctx.output;
            }
        };

        const react = transform(html, options);

        should.exist(react);
        called.should.be.true;
        should.exist(node);
        output.__text.should.equal('Text');
    });

    it('aborts when visitText returns false', () => {
        const html = '<div>Text</div>';
        const expected = '<div></div>';
        const options = {
            visitText: () => {
                return false;
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('honors changes when visitText modifies output', () => {
        const html = '<div>Text</div>';
        const expected = '<div>New Text</div>';
        const options = {
            visitText: (ctx) => {
                ctx.output = 'New Text';
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('calls visitProp', () => {
        let called = false;
        let node;
        let output;
        const html = '<div data-bind="binding">Text</div>';
        const options = {
            visitProp: (ctx) => {
                called = true;
                node = ctx.node;
                output = ctx.output;
            }
        };

        const react = transform(html, options);

        should.exist(react);
        called.should.be.true;
        should.exist(node);
        should.exist(output);
    });

    it('aborts when visitProp returns false', () => {
        const html = '<div data-bind="binding"></div>';
        const expected = '<div></div>';
        const options = {
            visitProp: () => {
                return false;
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('honors changes when visitProp modifies output', () => {
        const html = '<div data-bind="binding">Text</div>';
        const expected = '<div data-bind="new-binding">Text</div>';
        const options = {
            visitProp: (ctx) => {
                ctx.output.value = 'new-binding';
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('style attribute calls visitProp', () => {
        let called = false;
        let node;
        let output;
        const html = '<div style="color:#000;background-color:#fff;">Text</div>';
        const options = {
            visitProp: (ctx) => {
                called = true;
                node = ctx.node;
                output = ctx.output;
            }
        };

        const react = transform(html, options);

        should.exist(react);
        called.should.be.true;
        should.exist(node);
        should.exist(output);
    });

    it('calls visitStyle', () => {
        let called = false;
        let node;
        let output;
        const html = '<div style="color:#000;background-color:#fff;">Text</div>';
        const options = {
            visitStyle: (ctx) => {
                called = true;
                node = ctx.node;
                output = ctx.output;
            }
        };

        const react = transform(html, options);

        should.exist(react);
        called.should.be.true;
        should.exist(node);
        should.exist(output);
    });

    it('aborts when visitStyle returns false', () => {
        const html = '<div style="color:#000;background-color:#fff"></div>';
        const expected = '<div style="background-color:#fff"></div>';
        const options = {
            visitStyle: (ctx) => {
                if (ctx.output.key === 'color') return false;
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });

    it('honors changes when visitStyle modifies output', () => {
        const html = '<div style="color:#000;background-color:#fff"></div>';
        const expected = '<div style="color:#fff;background-color:#fff"></div>';
        const options = {
            visitStyle: (ctx) => {
                if (ctx.output.key === 'color') {
                    ctx.output.value = '#fff';
                }
            }
        };

        const react = transform(html, options);

        const element = createElements(react);
        const renderedHtml = ReactDOMServer.renderToStaticMarkup(element);

        should.exist(react);
        renderedHtml.should.equal(expected);
    });
});
