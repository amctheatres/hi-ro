import webpack from 'webpack';
import yargs from 'yargs';
import path from 'path';

export const options = yargs
    .alias('p', 'optimize-minimize')
    .argv;

var root = path.join(__dirname, '../');

const baseConfig = {
    entry: undefined,
    output: undefined,
    externals: undefined,
    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js'],
        root: root
    },
    module: {
        loaders:[
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel?cacheDirectory'
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(options.optimizeMinimize ? 'production' : 'development')
            }
        })
    ]
};

if (options.optimizeMinimize) {
    baseConfig.devtool = 'source-map';
}

export default baseConfig;