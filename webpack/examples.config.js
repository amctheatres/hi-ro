import path from 'path';
import baseConfig, { options } from './base.config';
var root = path.join(__dirname, '../');
import webpack from 'webpack';

export default Object.assign({}, baseConfig, {
    entry: {
        client: 'webpack-dev-server/client?http://localhost:8080/',
        app: [path.join(root, 'examples/src/app.js'), 'webpack/hot/only-dev-server']

    },

    output: {
        path: path.join(root, 'examples/js'),
        filename: options.optimizeMinimize ? '[name].min.js' : '[name].js',
        publicPath: '/js/'
    },
    devtool: 'source-map',
    externals: [
        {
            'jsdom': {
                root: 'jsdom',
                commonjs2: 'jsdom',
                commonjs: 'jsdom',
                amd: 'jsdom'
            }
        }
    ],
    module: {
        loaders:[
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['babel?cacheDirectory']
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(options.optimizeMinimize ? 'production' : 'development')
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
});